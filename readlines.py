import urllib2
import base64
import re
from operator import itemgetter

# username = "admin"
# password = "Cyberl1nk"
#
# request = urllib2.Request("http://api.qubicle.id/logs1/?file=nginx/access-api.log")
# base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
# request.add_header("Authorization", "Basic %s" % base64string)
# urllog = urllib2.urlopen(request)
#
# file = urllog.read().replace("<br>","\n")
# fw = open("out.log","w")
# fw.write(file)
# fw.close()


def process_log(log):
    requests = get_requests(log)
    files = get_files(requests)
    totals = file_occur(files)
    return totals

def get_requests(f):
    log_line = f.read()
    # logf = open(f)
    # log_line = logf.readline()
    pat = (r''
           '(\d+.\d+.\d+.\d+)\s-\s-\s'
           '\[\s?(\d+/\D+?/.*?)\]\s'
           '(\d+.\d*)\s' # or use (\".*?\"|\[.*?\]|\S+)+\s
           '\"(\w{3,6}.* \w{0,4}/\d\.\d)\"\s'
           '(\d+)\s' #status
           '(\d+)\s' #bandwidth
        )
    requests = find(pat, log_line)
    return requests

def find(pat, text):
    match = re.findall(pat, text)
    if match:
        return match
    return False


def get_files(requests):
    #get requested files with req
    requested_files = []
    for req in requests:
        requested_files.append(req[2]) ## get list
        # if (requested_files >= 3):

    return requested_files

def file_occur(files):
    # file occurrences in requested files
    d = {}
    for file in files:
        d[file] = d.get(file,0)+1
    return d

if __name__ == '__main__':
    log_file = open('out.log', 'r')
    # return dict of files and total requests
    urls_with_counts = process_log(log_file)
    # sort them by total requests d escending
    sorted_by_count = sorted(urls_with_counts.items(), key=itemgetter(1), reverse=True)
    print(sorted_by_count)



