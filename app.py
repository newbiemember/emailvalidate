__author__ = "Giri Rahayu"
__written_date__ = "Okt 2016"
__title__ = "Email Validation"
__email__ = "giri@codigo.id"

from globalFunction import *
import web
import json


web.config.debug = False
urls = (
	'/','Default',
    '/emailvalidate/','EmailValidated',
    '/statusheader/','GetStatusHeader',
    '/dglogprocess/','GetHtmlParseDglog',
)


class Default(object):
    def GET(self):
        output = {'author': "@newbiemember",
                  'mailsupport': "infra@codigo.id",
                  'status': 204,
                  'description': "No Content"}

        web.header('Content-Type', 'Application/json')
        web.header('Mail-Support', 'infra@codigo.id')
        web.header('Powered-By', 'Web.py')
        return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))

class EmailValidated(object):
    def GET(self):
        get = web.input(email="",detail="")
        if not (get.email):
            output = {'respone': "What are you looking for ??",
                      'status': 200

            }
            web.header('Content-Type', 'Application/json')
            web.header('Mail-Support', 'infra@codigo.id')
            web.header('Powered-By', 'Web.py')
            return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))
        else:

            return EmailValidate().emailcheck(get.email,get.detail)

class GetStatusHeader(object):
    def GET(self):
        get = web.input(domain="")
        if not (get.domain):
            output = {'respone': "What are you looking for ??",
                      'status': 200

            }
            web.header('Content-Type', 'Application/json')
            web.header('Mail-Support', 'infra@codigo.id')
            web.header('Powered-By', 'Web.py')
            return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))
        else:
            return GetStatus().cekdomain(get.domain)

class GetHtmlParseDglog(object):
    def GET(self):
        get = web.input(page="",element="")
        if not (get.page):
            output = {'respone': "What are you looking for ??",
                      'status': 200

            }
            web.header('Content-Type', 'Application/json')
            web.header('Mail-Support', 'infra@codigo.id')
            web.header('Powered-By', 'Web.py')
            return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))
        else:
            return GetParseDglog().getEmelementParse(get.page)


app = web.application(urls, globals()).wsgifunc()

# if __name__ == "__main__":
#     app = web.application(urls, globals())
#     app.run()



    

